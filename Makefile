CXX = clang++
CXX_FLAGS = -Ideps/ -Wall -lcurl -lstdc++ -std=c++17

BUILD_PATH = build
BIN_PATH = $(BUILD_PATH)/bin

OBJECTS = $(BUILD_PATH)/base.o $(BUILD_PATH)/file.o $(BUILD_PATH)/trello.o $(BUILD_PATH)/string.o $(BUILD_PATH)/text.o
DEBUG = src/debug.h

.PHONY: release
release: export CXX_FLAGS := $(CXX_FLAGS) -O2 -D RELEASE
release: $(BIN_PATH)/honoka

.PHONY: debug
debug: $(BIN_PATH)/honoka

dirs: $(BUILD_PATH) $(BIN_PATH)
$(BUILD_PATH):
	@mkdir $(BUILD_PATH)
$(BIN_PATH):
	@mkdir $(BIN_PATH)

###
# The backends
$(BUILD_PATH)/base.o: src/backend/base.cpp src/backend/base.h $(DEBUG)
	$(CXX) $(CXX_FLAGS) \
		src/backend/base.cpp \
		-c -o $(BUILD_PATH)/base.o
$(BUILD_PATH)/trello.o: $(BUILD_PATH)/string.o src/backend/trello.cpp src/backend/trello.h $(DEBUG)
	$(CXX) $(CXX_FLAGS) \
		src/backend/trello.cpp $(BUILD_PATH)/string.o \
		-c -o $(BUILD_PATH)/trello.o
$(BUILD_PATH)/file.o: src/backend/file.cpp src/backend/file.h $(DEBUG)
	$(CXX) $(CXX_FLAGS) \
		src/backend/file.cpp \
		-c -o $(BUILD_PATH)/file.o

$(BUILD_PATH)/text.o: src/frontend/text.h src/frontend/text.cpp
	$(CXX) $(CXX_FLAGS) \
		src/frontend/text.cpp \
		-c -o $(BUILD_PATH)/text.o

$(BUILD_PATH)/string.o: src/util/string.cpp src/util/string.h
	$(CXX) $(CXX_FLAGS) \
		src/util/string.cpp \
		-c -o $(BUILD_PATH)/string.o

$(BIN_PATH)/honoka: dirs src/config.cpp src/main.cpp src/config.h src/array.hpp src/debug.h $(OBJECTS) $(DEBUG)
	$(CXX) $(CXX_FLAGS) \
		src/config.cpp src/main.cpp $(OBJECTS) \
		-o $(BIN_PATH)/honoka

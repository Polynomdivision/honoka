#include <fstream>
#include <cstdlib>
#include <json.hpp>
#include "backend/file.h"
#include "backend/trello.h"
#include "config.h"

using json = nlohmann::json;

Config::Config() {
  std::ifstream f(DEFAULT_CONFIG_PATH);
  json data;
  f >> data;

  f.close();
}

Config::Config(std::string path) {
  std::ifstream f(path);
  json data;
  f >> data;

  auto backends = data.at("backends");
  
  // Configure files
  if (!backends["file"].is_null()) {
    this->file = optional<FileBackend>{FileBackend(FileConfig{
                                                    backends["file"]
                                                      })};
  }

  // Configure trello
  auto trello = backends["trello"];
  if (!trello.is_null()) {
    this->trello = optional<TrelloBackend>{TrelloBackend({
                                                          trello["key"],
                                                          trello["token"],
                                                          vector<string>(trello["doneListNames"])
                                                            })};
  }

  auto frontend = data["frontend"];
  if (!frontend.is_null()) {
    // TODO:
    // Dunno. Set a frontend
  } else {
    // Fallback to text
    this->text = TextFrontend{};
  }
  
  f.close();
}

BaseFrontend *Config::getFrontend() {
  return &(*(this->text));
}

vector<BaseBackend *> Config::getBackends() {
  vector<BaseBackend *> backends;

  if (this->file != nullopt)
    backends.push_back((BaseBackend *) &(this->file));
  if (this->trello != nullopt)
    backends.push_back((BaseBackend *) &(this->trello));

  return backends;
}

// Check if we can use a backend
bool Config::isConfigured(BackendType type) {
  // Why even bother (TODO?)
  if (type == BACKEND_BASE) return true;

  if (type == BACKEND_FILE) {
    return this->file != nullopt; 
  } else if (type == BACKEND_TRELLO) {
    return this->trello != nullopt;
  }

  return false;
}

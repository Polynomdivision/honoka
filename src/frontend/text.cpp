#include <iostream>
#include <rang.hpp>
#include "text.h"
#include "../util/string.h"
#include "../symbols.h"

void TextFrontend::renderTask(Task &task) {
  // Print the header
  cout << rang::style::underline
       << rang::fg::gray
       << task.title
       << rang::style::reset
       << " ("
       << taskStateToSymColor(task.state)
       << taskStateToSym(task.state)
       << rang::fg::reset
       << " )";
  if (task.isStarred)
    cout << " " << rang::fg::yellow
         << EYE
         << rang::fg::reset;
  cout << endl;

  // Print the description
  cout << endl << task.desc << endl;
}

void TextFrontend::renderBoard(Board &board, BaseBackend *backend) {
  auto tasks = backend->getTasks(board.id);

  // Longest id
  int max_id_length = 0;

  // Get the amount of finished and unfinished tasks
  int todos = 0;
  int dones = 0;
  for (auto &task : tasks) {
    // Find the longest id
    if (task.idShort.size() > max_id_length)
      max_id_length = task.idShort.size();
    
    switch(task.state) {
    case TASK_DONE:
      dones++;
      break;
    case TASK_TODO:
      todos++;
      break;
    case TASK_UNKNOWN:
      break;
    }
  }

  string header_suffix = "";
  if (board.type == BOARD_TRELLO)
    header_suffix = " (trello)";
  
  // Print the header
  cout << rang::style::underline
            << rang::fg::gray
            << board.name
            << rang::style::reset
            << header_suffix
            << " [" << tasks.size() << "/" << dones << "]"
            << endl;

  for (auto &task : tasks) {
    cout << rang::fgB::gray << "  " << honoka::rightPad(task.idShort + ". ", " ", max_id_length + 2)
              << taskStateToSymColor(task.state)
              << taskStateToSym(task.state) << "  "
              << rang::fg::reset
              << task.title;

    // Highlight tasks that are assigned to you
    if (task.assignedToYou)
      cout << rang::fg::red << " (X)"
           << rang::fg::reset;

    // Highlight tasks that are starred
    if (task.isStarred) {
      if (backend->type == BACKEND_TRELLO)
        cout << rang::fg::yellow << " " << EYE;
      else
        cout << rang::fg::yellow << " " << STAR;
    }
    
    // Don't forget the newline
    cout << endl;
  }

  // Display the percentage of done tasks
  // No rounding. We just abuse the fact that we can only store numbers element
  // |Z in an int. Just lose all those decimal places
  int p = ((double) dones / (double) tasks.size()) * 100;

  cout << endl << "  "
       << p << "% of all tasks done." << endl;
  cout << "  " << rang::fg::green << dones << rang::fg::reset
       << " done" << SEPARATOR
       << rang::fg::magenta << todos << rang::fg::reset
       << " pending" << endl;
}

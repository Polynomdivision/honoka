#pragma once
#include "base.h"

class TextFrontend : public BaseFrontend {
 public:
  void renderBoard(Board &board, BaseBackend *backend);
  void renderTask(Task &task);
};

#pragma once
#include "../backend/base.h"

class BaseFrontend {
 public:
  virtual void renderBoard(Board &board, BaseBackend *backend) = 0;
  virtual void renderTask(Task &task) = 0;
};

template <typename T>
class Option {
public:
  Option(T *ptr);

  bool valid() {
    return this->data_ptr != 0;
  }

  T get() {
    return *(this->data_ptr);
  }

  void set (T* ptr) {
    this->data_ptr = ptr;
  }
private:
  T *data_ptr;
};

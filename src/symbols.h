#pragma once
#include "backend/base.h"
#include <rang.hpp>

using namespace std;

#define DONE_SYM "✓"
#define TODO_SYM "☐"
#define SEPARATOR " · "
#define STAR "★"
#define EYE "👁"

// Convert a task state into its appropriate symbol
string taskStateToSym(TaskState state) {
  switch(state) {
  case TASK_DONE: return DONE_SYM;
  case TASK_TODO: return TODO_SYM;
  case TASK_UNKNOWN: return "x";
  }
}

// Convert a task state into its appropriate colour
rang::fg taskStateToSymColor(TaskState state) {
  switch(state) {
  case TASK_DONE: return rang::fg::green;
  case TASK_TODO: return rang::fg::magenta;
  case TASK_UNKNOWN: return rang::fg::gray; 
  }
}

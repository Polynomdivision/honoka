#pragma once

#include <cstdlib>
#include <string>
#include <vector>
#include <optional>
#include "backend/file.h"
#include "backend/trello.h"
#include "frontend/text.h"

using namespace std;

#ifndef RELEASE
// For better "testing", we will keep a config in the repositories' root
#define DEFAULT_CONFIG_PATH "config.json"

#else
#define DEFAULT_CONFIG_PATH string(getenv("HOME")) + "/.config/honoka/config.json"

#endif

class Config {
 public:
  Config();
  Config(string path);

  bool isConfigured(BackendType type);

  vector<BaseBackend *> getBackends();
  BaseFrontend *getFrontend();
private:
  optional<TrelloBackend> trello = nullopt;
  optional<FileBackend> file = nullopt;
  optional<TextFrontend> text = nullopt;
};

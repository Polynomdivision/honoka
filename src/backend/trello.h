#pragma once
#include <curl/curl.h>
#include "base.h"

struct TrelloConfig {
  string trelloKey;
  string trelloToken;
  vector<string> trelloDoneListNames;
};

class TrelloBackend : public BaseBackend {
 public:
  TrelloBackend(TrelloConfig cfg);
  ~TrelloBackend();
  
  vector<Board> getBoards();
  optional<Board> getBoard(string id);
  optional<Board> getBoardByName(string name);

  vector<Task> getTasks(string id);
  optional<Task> getTaskByIdShort(string boardId, string taskId);
  void addTask(string boardId, string title, string desc);
  void markAs(string boardId, string taskId, TaskState state);
  void removeTask(string boardId, string taskId);

  void createBoard(string boardName);
  void removeBoard(string boardName);

  void starTask(string boardName, string taskId);
  void unstarTask(string boardName, string taskId);

 private:
  optional<string> get(const string &url);
  optional<string> put(const string &url);
  optional<string> post(const string &url, const string &postData);
  optional<string> http_delete(const string &url);

  optional<string> getUserId();
  optional<string> userId = nullopt;
  
  CURL *curl;
  TrelloConfig cfg;
};

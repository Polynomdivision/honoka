#include <fstream>
#include <iostream>
#include "file.h"
#include "../array.hpp"
#include "../debug.h"

FileBackend::FileBackend(FileConfig cfg) {
  this->path = cfg.fileBackendPath;
  this->type = BACKEND_FILE;
  this->cfg = cfg;

  std::ifstream file(this->path);
  file >> this->data;
  file.close();
}

// Return the known boards
std::vector<Board> FileBackend::getBoards() {
  vector<Board> boards;
  for (auto &board : this->data.at("boards")) {
    boards.push_back({
                   board["name"],
                   board["id"],
                   BOARD_FILE
    });
  }

  return boards;
}

std::optional<Board> FileBackend::getBoard(std::string id) {
  auto boards = this->data.at("boards");
  for (auto &board : boards) {
    if (board["id"] != id) continue;

    BoardType type = strToBoardType(board["type"]);
    
    return std::optional<Board>{{ board["name"], board["id"], type }};
  }

  return std::nullopt;
}

std::optional<Board> FileBackend::getBoardByName(std::string name) {
  for (auto &board : this->getBoards()) {
    if (board.name != name) continue;
    return std::optional<Board>{board};
  }

  return std::nullopt;
}

//Return the cards of a given board
std::vector<Task> FileBackend::getTasks(std::string id) {
  auto tasks = this->data.at("tasks");
  std::vector<Task> ret;

  for (auto &task : tasks) {
    // Ignore tasks of other boards
    if (task["boardId"] != id) continue;

    TaskState state = strToTaskState(task["state"]);
    ret.push_back({
      task["title"],
      task["desc"],
      task["id"],
      task["id"],
      task["boardId"],
      true,
      task["starred"],
      state
    });
  }

  return ret;
}

std::optional<Task> FileBackend::getTaskByIdShort(std::string boardId, std::string taskId) {
  auto tasks = this->getTasks(boardId);
  for (auto &task : tasks)
    if (task.idShort == taskId)
      return std::optional<Task>{task};

  return std::nullopt;
}

std::string FileBackend::genTaskID(std::string boardId) {
  // Find the biggest ID and increment it
  unsigned int id = 0;
  for (auto &task : this->data.at("tasks")) {
    std::string taskId (task["id"]);
    if (task["boardId"] == boardId && std::stoi(taskId) > id)
      id = std::stoi(taskId);
  }

  return std::to_string(id + 1);
}


void FileBackend::addTask(std::string boardId, std::string title, std::string desc) {
  _SEMI_STUB("No tasks are starred");
  std::string id = this->genTaskID(boardId);

  auto tasks = this->data.at("tasks");
  tasks.push_back(taskToJson({title, desc, id, id, boardId, true, false, TASK_TODO}));
  this->data["tasks"] = tasks;

  this->sync();
}

// Writes the data to the json file
void FileBackend::sync() {
  std::ofstream file(this->path);
  file << this->data.dump(4);
  file.close();
}

void FileBackend::markAs(std::string boardId, std::string taskId, TaskState state) {
  auto tasks = this->data.at("tasks");
  // Try to find the correct task
  for (unsigned int i = 0; i < tasks.size(); i++) {
    if (tasks.at(i)["boardId"] == boardId && tasks.at(i)["id"] == taskId) {
      this->data.at("tasks").at(i)["state"] = taskStateToStr(state);
      this->sync();

      return;
    }
  }
}

void FileBackend::removeTask(std::string boardId, std::string taskId) {
   auto tasks = this->data.at("tasks");
  // Try to find the correct task
  for (unsigned int i = 0; i < tasks.size(); i++) {
    if (tasks.at(i)["boardId"] == boardId && tasks.at(i)["id"] == taskId) {
      this->data.at("tasks").erase(i);
      this->sync();

      return;
    }
  }
}

std::string FileBackend::genBoardID() {
  unsigned int max_id = 0;
  for(auto &board : this->data.at("boards")) {
    std::string id (board["id"]);
    if (std::stoi(id) > max_id)
      max_id = std::stoi(id);
  }

  return std::to_string(max_id + 1);
}

void FileBackend::createBoard(std::string boardName) {
  // We wont create a board, if one with the same name also exists
  if(this->getBoardByName(boardName) != std::nullopt) return;

  json board;
  board["id"] = this->genBoardID();
  board["name"] = boardName;
  board["type"] = "file";

  this->data.at("boards").push_back(board);
  this->sync();
}

void FileBackend::removeBoard(std::string boardName) {
  // We won't try to remove a board that isn't there
  std::optional<Board> board = this->getBoardByName(boardName);
  if(board == std::nullopt) return;

  // Find the index of the board and remove its entry
  for (json::iterator it = this->data.at("boards").begin();
       it != this->data.at("boards").end(); it++) {

    if ((*it)["id"] == board->id) {
      it = this->data.at("boards").erase(it);
      break;
    }
  }

  // Remove all tasks that are on the board
  for (json::iterator it = this->data.at("tasks").begin();
       it != this->data.at("tasks").end(); it++) {
    
    // Check if the task is belonging to the board we're removing
    if (it->at("boardId") == board->id) {
      it = this->data.at("tasks").erase(it);
    }
  }

  this->sync();
}

void FileBackend::starTask(string boardName, string taskId) {
  // Find the board
  auto board = this->getBoardByName(boardName);
  if (board == nullopt) {
    cout << "Failed to find the board!" << endl;
    return;
  }

  // Find the task
  for (auto &task : this->data.at("tasks")) {
    if (task["boardId"] == board->id && task["id"] == taskId)
      task["starred"] = true;
  }

  this->sync();
}

void FileBackend::unstarTask(string boardName, string taskId) {
  // Find the board
  auto board = this->getBoardByName(boardName);
  if (board == nullopt) {
    cout << "Failed to find the board!" << endl;
    return;
  }

  // Find the task
  for (auto &task : this->data.at("tasks")) {
    if (task["boardId"] == board->id && task["id"] == taskId)
      task["starred"] = false;
  }

  this->sync();
}

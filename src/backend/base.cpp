#include <cstring>
#include "base.h"

using namespace std;

TaskState strToTaskState(string str) {
  if (str == "todo") {
    return TASK_TODO;
  } else if (str == "done") {
    return TASK_DONE;
  } else {
    return TASK_UNKNOWN;
  }
}

string taskStateToStr(TaskState state) {
  switch(state) {
  case TASK_TODO: return "todo";
  case TASK_DONE: return "done";
  case TASK_UNKNOWN: return "unknown";
  }
}

BoardType strToBoardType(string str) {
  if (str == "file") {
    return BOARD_FILE;
  } else if (str == "trello") {
    return BOARD_TRELLO;
  } else {
    return BOARD_UNKNOWN;
  }
}

json taskToJson(Task task) {
  json data;
  data["boardId"] = task.boardId;
  data["desc"] = task.desc;
  data["id"] = task.id;
  data["state"] = taskStateToStr(task.state);
  data["title"] = task.title;
  data["starred"] = task.isStarred;

  return data;
}

optional<BoardMetadata> findBoardByName(string name, const vector<BaseBackend *> &backends) {
  for (auto &backend : backends) {
    auto board = backend->getBoardByName(name);
    if (board != nullopt)
      return optional<BoardMetadata>{{*board, backend}};
  }

  return nullopt;
}

vector<Board> getBoards(const vector<BaseBackend *> &backends) {
  vector<Board> boards;

  for (auto &backend : backends)
    for (auto &board : backend->getBoards())
      boards.push_back(board);

  return boards;
}

pair<string, BackendType> getBoardNameAndType(string input) {
  string boardName = input.substr(2);
  BackendType type;

  if (strncmp(input.c_str(), "t", 1) == 0) {
    type = BACKEND_TRELLO;
  } else if (strncmp(input.c_str(), "f", 1) == 0){
    type = BACKEND_FILE;
  } else {
    type = BACKEND_BASE;
  }
  
  return make_pair(boardName, type);
}

optional<BaseBackend *> getBackend(const vector<BaseBackend *> &backends, BackendType type) {
  for (auto &backend_ptr : backends)
    if (backend_ptr->type == type)
      return optional<BaseBackend *>{backend_ptr};
  
  return nullopt;
}

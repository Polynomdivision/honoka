#pragma once

#include <string>
#include <vector>
#include <optional>
#include <utility>
#include "../../deps/json.hpp"

using json = nlohmann::json;
using namespace std;

enum BoardType {
                BOARD_UNKNOWN,
                BOARD_FILE,
                BOARD_TRELLO
};

struct Board {
  string name;
  string id;
  BoardType type;
};

BoardType strToBoardType(string str);

enum TaskState {
                TASK_TODO,
                TASK_DONE,
                TASK_UNKNOWN
};

TaskState strToTaskState(string str);
string taskStateToStr(TaskState state);

struct Task {
  string title;
  string desc;
  string id;
  string idShort;
  string boardId;
  bool assignedToYou;
  bool isStarred;
  TaskState state;
};

json taskToJson(Task task);

enum BackendType {
                  BACKEND_BASE,
                  BACKEND_FILE,
                  BACKEND_TRELLO
};

class BaseBackend {
 public:
  virtual vector<Board> getBoards() = 0;
  virtual optional<Board> getBoard(string id) = 0;
  virtual optional<Board> getBoardByName(string name) = 0;

  virtual vector<Task> getTasks(string id) = 0;
  virtual optional<Task> getTaskByIdShort(string boardId, string taskId) = 0;
  
  virtual void addTask(string id, string title, string desc) = 0;
  virtual void markAs(string boardId, string taskId, TaskState state) = 0;
  virtual void removeTask(string boardId, string taskId) = 0;

  virtual void createBoard(string boardName) = 0;
  virtual void removeBoard(string boardName) = 0;

  virtual void starTask(string boardName, string taskId) = 0;
  virtual void unstarTask(string boardName, string taskId) = 0;
  
  BackendType type;
};

struct BoardMetadata {
  Board board;
  BaseBackend *backend;
};


pair<string, BackendType> getBoardNameAndType(string input);
optional<BoardMetadata> findBoardByName(string name, const vector<BaseBackend *> &backends);
vector<Board> getBoards(const vector<BaseBackend *> &backends);

#define NOT_NULLOPT(a, msg) if (a == nullopt) { \
  cout << msg << endl; \
  return 1; \
}

#define IS_NULLOPT(a, msg) if (a != nullopt) { \
  cout << msg << endl; \
  return 1; \
}

optional<BaseBackend *> getBackend(const vector<BaseBackend *> &backends, BackendType type);

#pragma once

#include <json.hpp>
#include "base.h"

using json = nlohmann::json;
using namespace std;

struct FileConfig {
  string fileBackendPath;
};

class FileBackend : public BaseBackend {
 public:
  FileBackend(FileConfig cfg);

  vector<Board> getBoards();
  optional<Board> getBoard(string id);
  optional<Board> getBoardByName(string name);

  vector<Task> getTasks(string id);
  optional<Task> getTaskByIdShort(string boardId, string taskId);
  
  void addTask(string boardId, string title, string desc);
  void markAs(string boardId, string taskId, TaskState state);
  void removeTask(string boardId, string taskId);

  void createBoard(string boardName);
  void removeBoard(string boardName);

  void starTask(string boardName, string taskId);
  void unstarTask(string boardName, string taskId);
 private:
  string genTaskID(string boardId);
  string genBoardID();
  void sync();
  
  string path;
  FileConfig cfg;
  json data;
};

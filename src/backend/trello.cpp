#include <iostream>
#include <cstring>
#include <iterator>
#include <algorithm>
#include <map>
#include <utility>
#include <json.hpp>
#include "trello.h"
#include "../array.hpp"
#include "../util/string.h"
#include "../debug.h"

// TODO: Consider using the /batch endpoint

using json = nlohmann::json;
using namespace std;

#define TRELLO_BASE_URL "https://api.trello.com/1/"
// Returns the full URL used to contact the @endpoint.
// @endpoint must not start with a slash (/).
// TODO: Convert this into a macro
string trelloUrl(string endpoint, string urlParams, TrelloConfig &cfg) {
  return TRELLO_BASE_URL + endpoint + "?" + urlParams + "&key=" + cfg.trelloKey + "&token=" + cfg.trelloToken;
}

TrelloBackend::TrelloBackend(TrelloConfig cfg) {
  this->cfg = cfg;
  this->type = BACKEND_TRELLO;

  // Init curl
  curl_global_init(CURL_GLOBAL_DEFAULT);
}

TrelloBackend::~TrelloBackend() {
  // Cleanup of curl
  curl_global_cleanup();
}

optional<string> TrelloBackend::getUserId() {
  // Fetch the user ID
  optional<string> data = this->get(trelloUrl("members/me", "", cfg));
  if (data == nullopt)
    return nullopt;
  else
    return optional<string>{json::parse(*data)["id"]};
}

// Copy the data that libcurl received into s
size_t writeToString(void *contents, size_t size, size_t nmemb, string *s)
{
    size_t newLength = size * nmemb;
    size_t oldLength = s->size();
    try
    {
        s->resize(oldLength + newLength);
    }
    catch(bad_alloc &e)
    {
        //handle memory problem
        return 0;
    }

    copy((char *) contents, (char *) contents+newLength, s->begin() + oldLength);
    return size*nmemb;
}

vector<Board> TrelloBackend::getBoards() {
  optional<string> data = this->get(trelloUrl("members/me/boards", "", this->cfg));

  if (data == nullopt) {
    cout << "Failed to get boards!" << endl;
    return {};
  }
  
  vector<Board> boards;
  json j = json::parse(*data);
  for (auto &board : j) {
    boards.push_back({board["name"], board["id"], BOARD_TRELLO});
  }

  return boards;
}

// Perform a GET-request to the given url
optional<string> TrelloBackend::get(const string &url) {
  DEBUG("GET: " + url);

  this->curl = curl_easy_init();
  
  string data;
  curl_easy_setopt(this->curl, CURLOPT_URL, url.c_str());
  curl_easy_setopt(this->curl, CURLOPT_FAILONERROR, 1L);
  curl_easy_setopt(this->curl, CURLOPT_SSL_VERIFYPEER, 0L); //only for https
  curl_easy_setopt(this->curl, CURLOPT_SSL_VERIFYHOST, 0L); //only for https
  curl_easy_setopt(this->curl, CURLOPT_WRITEFUNCTION, writeToString);
  curl_easy_setopt(this->curl, CURLOPT_WRITEDATA, &data);

  CURLcode res = curl_easy_perform(this->curl);
  if (res != CURLE_OK) {
    cout << "Could not perform GET-request!" << endl;
    DEBUG(data);
    return nullopt;
  }

//   if (data.find("invalid") != string::npos) {
//     cout << "Invalid Trello token or API-Key!" << endl;
//     return nullopt;
//   }
 
  curl_easy_cleanup(this->curl);
  return optional<string>{data};
}

optional<string> TrelloBackend::post(const string &url, const string &postData) {
  DEBUG("POST: " + url);
  DEBUG("POST Data: " + postData);

  this->curl = curl_easy_init();

  string data;
  curl_easy_setopt(this->curl, CURLOPT_URL, url.c_str());
  curl_easy_setopt(this->curl, CURLOPT_POSTFIELDS, postData.c_str());
  curl_easy_setopt(this->curl, CURLOPT_SSL_VERIFYPEER, 0L); //only for https
  curl_easy_setopt(this->curl, CURLOPT_SSL_VERIFYHOST, 0L); //only for https
  curl_easy_setopt(this->curl, CURLOPT_WRITEFUNCTION, writeToString);
  curl_easy_setopt(this->curl, CURLOPT_WRITEDATA, &data);

  CURLcode res = curl_easy_perform(this->curl);
  if (res != CURLE_OK) {
    cout << "Could not perform POST-Request!" << endl;
    DEBUG(data);
    return nullopt;
  }
  
  curl_easy_cleanup(this->curl);
  return optional<string>{data};
}

optional<string> TrelloBackend::http_delete(const string &url) {
  DEBUG("DELETE: " + url);

  this->curl = curl_easy_init();

  string data;
  curl_easy_setopt(this->curl, CURLOPT_URL, url.c_str());
  curl_easy_setopt(this->curl, CURLOPT_CUSTOMREQUEST, "DELETE");
  curl_easy_setopt(this->curl, CURLOPT_SSL_VERIFYPEER, 0L); //only for https
  curl_easy_setopt(this->curl, CURLOPT_SSL_VERIFYHOST, 0L); //only for https
  curl_easy_setopt(this->curl, CURLOPT_WRITEFUNCTION, writeToString);
  curl_easy_setopt(this->curl, CURLOPT_WRITEDATA, &data);

  CURLcode res = curl_easy_perform(this->curl);
  if (res != CURLE_OK) {
    cout << "Could not perform DELETE-Request!" << endl;
    DEBUG(data);
    return nullopt;
  }
  
  curl_easy_cleanup(this->curl);
  return optional<string>{data};
}

optional<string> TrelloBackend::put(const string &url) {
  DEBUG("PUT: " + url);

  this->curl = curl_easy_init();

  string data;
  curl_easy_setopt(this->curl, CURLOPT_URL, url.c_str());
  curl_easy_setopt(this->curl, CURLOPT_CUSTOMREQUEST, "PUT");
  curl_easy_setopt(this->curl, CURLOPT_SSL_VERIFYPEER, 0L); //only for https
  curl_easy_setopt(this->curl, CURLOPT_SSL_VERIFYHOST, 0L); //only for https
  curl_easy_setopt(this->curl, CURLOPT_WRITEFUNCTION, writeToString);
  curl_easy_setopt(this->curl, CURLOPT_WRITEDATA, &data);

  CURLcode res = curl_easy_perform(this->curl);
  if (res != CURLE_OK) {
    cout << "Could not perform PUT-Request!" << endl;
    DEBUG(data);
    return nullopt;
  }
  
  curl_easy_cleanup(this->curl);
  return optional<string>{data};
}

optional<Board> TrelloBackend::getBoard(string id) {
  optional<string> data = this->get(trelloUrl("board/" + id, "", this->cfg));

  if (data == nullopt) {
    cout << "Failed to get board" << endl;
    return nullopt;
  }

  json j = json::parse(*data);
  return optional<Board>{{j["name"], j["id"], BOARD_TRELLO}};
}

optional<Board> TrelloBackend::getBoardByName(string name) {
  vector<Board> boards = this->getBoards();

  for (auto &board : boards)
    if (board.name == name)
      return optional<Board>{board};
  
  return nullopt;
}

vector<Task> TrelloBackend::getTasks(string id) {
  vector<Task> tasks;
  string urls = "urls=/boards/" + id + "/cards,/boards/" + id + "/cards/closed,/boards/" + id + "/lists";
  optional<string> data_raw = this->get(trelloUrl("batch", urls, this->cfg));
  if (data_raw == nullopt) {
    cout << "Could not get tasks and lists!" << endl;
    return {};
  }
  json data = json::parse(*data_raw);
  
  // Convert the list-array into a mapping from the ID to its name
  map<string, string> idToName;
  for (auto &list : data[2]["200"])
    idToName.emplace(make_pair(list["id"], list["name"]));

  // We need to request two sets of cards, over which we need to
  // iterate: Open and closed ones.
  for (int i = 0; i < 2; i++) {
    for (auto &task : data[i]["200"]) {
      // Check if the task is either closed or in a list that matches doneListNames
      TaskState state = TASK_TODO;
      if (task["closed"] == true) {
        state = TASK_DONE;
      } else {
        // Check if the name of the list is in the list of list names, which
        // indicate a completed task
        string name = idToName[task["idList"]];
        if (contains(name, this->cfg.trelloDoneListNames))
          state = TASK_DONE;
      }
    
      // Check if we are assigned to this task
      if (this->userId == nullopt)
        this->userId = this->getUserId();
      if (this->userId == nullopt) {
        cout << "Failed to fetch user ID" << endl;
        return {};
      }

      auto result = find(task["idMembers"].begin(), task["idMembers"].end(), this->userId);
      tasks.push_back({
                       task["name"],
                       task["desc"],
                       task["id"],
                       to_string((int) task["idShort"]),
                       task["idBoard"],
                       result != task["idMembers"].end(),
                       task["subscribed"],
                       state
        });
    }
  }
  
  return tasks;
}

optional<Task> TrelloBackend::getTaskByIdShort(string boardId, string taskId) {
  auto tasks = this->getTasks(boardId);
  for (auto &task : tasks)
    if (task.idShort == taskId)
      return optional<Task>{task};

  return nullopt;
}

void TrelloBackend::addTask(string boardId, string title, string desc) {
  // Get the lists on the board
  auto lists_raw = this->get(trelloUrl("boards/" + boardId + "/lists", "", this->cfg));
  if (lists_raw == nullopt) {
    cout << "Failed to get lists on board!" << endl;
    return;
  }

  cout << "Add to which list?" << endl
       << "Available lists: ";
  json lists = json::parse(*lists_raw);
  map<string, string> nameToId;
  vector<string> names;
  for (auto &board : lists) {
    nameToId[board["name"]] = board["id"];
    names.push_back(board["name"]);
    cout << board["name"] << ", ";
  }
  cout << endl;

  // Read a list name from stdin
  string listName;
  do {
    getline(cin, listName);
    cout << "? ";
  } while (!contains(listName, names));

  title = honoka::replace(title, " ", "%20");
  
  this->post("https://api.trello.com/1/cards", "key=" + this->cfg.trelloKey
             + "&token=" + this->cfg.trelloToken
             + "&idList=" + nameToId[listName]
             + "&name=" + title);
}

void TrelloBackend::removeTask(string boardId, string taskId) {
  auto tasks = this->getTasks(boardId);
  for (auto &task : tasks) {
    // Ignore other cards
    if (task.idShort != taskId)
      continue;

    this->http_delete("https://api.trello.com/1/cards/" + task.id
                      + "?key=" + this->cfg.trelloKey
                      + "&token=" + this->cfg.trelloToken);
    break;
  }
}

void TrelloBackend::markAs(string boardId, string taskId, TaskState state) {
  // As the ID is only the short ID, which is only unique on one board, we need
  // to extract the full ID first
  auto cards_raw = this->get(trelloUrl("boards/" + boardId + "/cards", "", this->cfg));
  if (cards_raw == nullopt) {
    cout << "Failed to get cards on board" << endl;
    return;
  }

  json cards = json::parse(*cards_raw);
  for (auto &card : cards) {
    // Ignore all other cards
    if (taskId != to_string((int) card["idShort"]))
      continue;

    // Found it
    string id = card["id"];
    string closed = state == TASK_DONE ? "&closed=true" : "&closed=false";

    this->put("https://api.trello.com/1/cards/" + id
              + "?key=" + this->cfg.trelloKey
              + "&token=" + this->cfg.trelloToken
              + closed);
    break;
  }
}

void TrelloBackend::createBoard(string boardName) {
  this->post("https://api.trello.com/1/boards/", "key=" + this->cfg.trelloKey
             + "&token=" + this->cfg.trelloToken
             + "&name=" + honoka::replace(boardName, " ", "%20"));
}

void TrelloBackend::removeBoard(string boardName) {
  auto board = this->getBoardByName(boardName);
  if (board == nullopt) {
    cout << "Failed to find board" << endl;
    return;
  }

  cout << "INFO: Board will be closed, not removed!" << endl;
  this->put("https://api.trello.com/1/boards/" + board->id
            + "?key=" + this->cfg.trelloKey
            + "&token=" + this->cfg.trelloToken
            + "&closed=true");
}

void TrelloBackend::starTask(string boardName, string taskId) {
  // Get the board
  auto board = this->getBoardByName(boardName);
  if (board == nullopt) {
    cout << "Failed to get board!" << endl;
    return;
  }

  // As the ID is only the short ID, which is only unique on one board, we need
  // to extract the full ID first
  auto cards_raw = this->get(trelloUrl("boards/" + board->id + "/cards", "", this->cfg));
  if (cards_raw == nullopt) {
    cout << "Failed to get cards on board" << endl;
    return;
  }

  json cards = json::parse(*cards_raw);
  for (auto &card : cards) {
    // Ignore all other cards
    if (taskId != to_string((int) card["idShort"]))
      continue;

    // Found it
    string id = card["id"];
    this->put("https://api.trello.com/1/cards/" + id
              + "?key=" + this->cfg.trelloKey
              + "&token=" + this->cfg.trelloToken
              + "&subscribed=true");
    break;
  }
}

void TrelloBackend::unstarTask(string boardName, string taskId) {
  // Get the board
  auto board = this->getBoardByName(boardName);
  if (board == nullopt) {
    cout << "Failed to get board!" << endl;
    return;
  }

  // As the ID is only the short ID, which is only unique on one board, we need
  // to extract the full ID first
  auto cards_raw = this->get(trelloUrl("boards/" + board->id + "/cards", "", this->cfg));
  if (cards_raw == nullopt) {
    cout << "Failed to get cards on board" << endl;
    return;
  }

  json cards = json::parse(*cards_raw);
  for (auto &card : cards) {
    // Ignore all other cards
    if (taskId != to_string((int) card["idShort"]))
      continue;

    // Found it
    string id = card["id"];
    this->put("https://api.trello.com/1/cards/" + id
              + "?key=" + this->cfg.trelloKey
              + "&token=" + this->cfg.trelloToken
              + "&subscribed=false");
    break;
  }
}

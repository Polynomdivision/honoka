#include <iostream>
#include <optional>
#include <cstring>
#include <cstdlib>
#include <memory>
#include <rang.hpp>
#include "backend/base.h"
#include "backend/file.h"
#include "backend/trello.h"
#include "config.h"
#include "util/string.h"
#include "frontend/base.h"
#include "debug.h"

void showAllBoards(BaseFrontend *front, Config &cfg) {
  for (auto &backend : cfg.getBackends()) {
    for (auto &board : backend->getBoards()) {
      front->renderBoard(board, backend);
      cout << endl;
    }
  }
}

int main(int argc, char **argv) {
  // Load the config
  Config cfg(DEFAULT_CONFIG_PATH);
  const vector<BaseBackend *> backends = cfg.getBackends();

  // Get the configured frontend
  BaseFrontend *front = cfg.getFrontend();
  
  // If we have no arguments, just show all boards
  if (argc == 1) {
    showAllBoards(front, cfg);
  } else {
    if (strcmp(argv[1], "a") == 0
        || strcmp(argv[1], "add") == 0) {
      // We expect at least 4 Arguments (3 without the path to the program
      // itself)
      if (argc <= 3) {
        cout << "Not enough arguments" << endl;
        return 1;
      }

      string title = honoka::concatUntilEnd(argv, argc, 3);

      auto data = getBoardNameAndType(argv[2]);
      auto backend = getBackend(backends, data.second);
      NOT_NULLOPT(backend, "Failed to find backend");

      auto board = (*backend)->getBoardByName(data.first);
      NOT_NULLOPT(board, "Failed to find board");

      (*backend)->addTask(board->id, title, "");
    } else if (strcmp(argv[1], "d") == 0
               || strcmp(argv[1], "done") == 0) {
      // We expect at least 4 Arguments (3 without the path to the program
      // itself)
      if (argc <= 3) {
        cout << "Not enough arguments" << endl;
        return 1;
      }

      auto data = getBoardNameAndType(argv[2]);
      auto backend = getBackend(backends, data.second);
      NOT_NULLOPT(backend, "Failed to find backend");

      auto board = (*backend)->getBoardByName(data.first);
      NOT_NULLOPT(board, "Failed to find board");

      (*backend)->markAs(board->id, argv[3], TASK_DONE);
    } else if (strcmp(argv[1], "t") == 0
               || strcmp(argv[1], "todo") == 0) {
      // We expect at least 4 Arguments (3 without the path to the program
      // itself)
      if (argc <= 3) {
        cout << "Not enough arguments" << endl;
        return 1;
      }

      auto data = getBoardNameAndType(argv[2]);
      auto backend = getBackend(backends, data.second);
      NOT_NULLOPT(backend, "Failed to find backend");

      auto board = (*backend)->getBoardByName(data.first);
      NOT_NULLOPT(board, "Failed to find board");

      (*backend)->markAs(board->id, argv[3], TASK_TODO);
    } else if (strcmp(argv[1], "r") == 0
               || strcmp(argv[1], "remove") == 0) {
      // We expect at least 4 Arguments (3 without the path to the program
      // itself)
      if (argc <= 3) {
        cout << "Not enough arguments" << endl;
        return 1;
      }

      auto data = getBoardNameAndType(argv[2]);
      auto backend = getBackend(backends, data.second);
      NOT_NULLOPT(backend, "Failed to find backend");

      auto board = (*backend)->getBoardByName(data.first);
      NOT_NULLOPT(board, "Failed to find board");

      (*backend)->removeTask(board->id, argv[3]);
    } else if (strcmp(argv[1], "c") == 0
               || strcmp(argv[1], "create") == 0) {
      if (argc != 3) {
        cout << "Two arguments expected." << endl;
        return 1;
      }

      auto data = getBoardNameAndType(argv[2]);
      auto backend = getBackend(backends, data.second);
      NOT_NULLOPT(backend, "Failed to find backend");

      auto board = (*backend)->getBoardByName(data.first);
      IS_NULLOPT(board, "Board already exists");

      (*backend)->createBoard(data.first);
    } else if (strcmp(argv[1], "rb") == 0) {
      if (argc != 3) {
        cout << "Two arguments expected." << endl;
        return 1;
      }

      auto data = getBoardNameAndType(argv[2]);
      auto backend = getBackend(backends, data.second);
      NOT_NULLOPT(backend, "Failed to find backend");

      (*backend)->removeBoard(data.first);
    } else if (strcmp(argv[1], "l") == 0
               || strcmp(argv[1], "list") == 0) {
      auto boards = getBoards(backends);

      for (auto &board : boards) {
        cout << board.name;

        if (board.type == BOARD_TRELLO)
          cout << " (trello)";

        cout << endl;
      }
    } else if (strcmp(argv[1], "s") == 0
               || strcmp(argv[1], "star") == 0) {
      // We expect at least 4 Arguments (3 without the path to the program
      // itself)
      if (argc <= 3) {
        cout << "Not enough arguments" << endl;
        return 1;
      }

      auto data = getBoardNameAndType(argv[2]);
      auto backend = getBackend(backends, data.second);
      NOT_NULLOPT(backend, "Failed to find backend!");

      (*backend)->starTask(data.first, argv[3]);
    } else if (strcmp(argv[1], "u") == 0
               || strcmp(argv[1], "unstar") == 0) {
      // We expect at least 4 Arguments (3 without the path to the program
      // itself)
      if (argc <= 3) {
        cout << "Not enough arguments" << endl;
        return 1;
      }

      auto data = getBoardNameAndType(argv[2]);
      auto backend = getBackend(backends, data.second);
      NOT_NULLOPT(backend, "Failed to find Backend");

      (*backend)->unstarTask(data.first, argv[3]);
    } else if (strcmp(argv[1], "v") == 0
               || strcmp(argv[1], "view") == 0) {
       // We expect at least 4 Arguments (3 without the path to the program
      // itself)
      if (argc <= 3) {
        cout << "Not enough arguments" << endl;
        return 1;
      }

      auto data = getBoardNameAndType(argv[2]);
      auto backend = getBackend(backends, data.second);
      if (backend == nullopt) {
        cout << "Failed to find the backend!" << endl;
        return 1;
      }

      auto board = (*backend)->getBoardByName(data.first);
      if (board == nullopt) {
        cout << "Failed to find board!" << endl;
        return 1;
      }

      auto task = (*backend)->getTaskByIdShort(board->id, argv[3]);
      if (task == nullopt) {
        cout << "Failed to find task!" << endl;
        return 1;
      }

      front->renderTask(*task);
    } else if (strcmp(argv[1], "h") == 0
               || strcmp(argv[1], "help") == 0) {
      cout << "honoka - Terminal task management" << endl
           << endl
           << "Usage: honoka <Verb> [Options]" << endl
           << endl
           << "Verbs:" << endl
           << "(none)   View boards" << endl
           << "         Options: <Board name> for a specific board, (none) for all" << endl
           << "a/add    Add a task to a board" << endl
           << "         Options: <Board type>:<Board name> <Title>" << endl
           << "d/done   Mark a task as done" << endl
           << "         Options: <Board type>:<Board name> <Task ID>" << endl
           << "t/todo   Mark a task as to do" << endl
           << "         Options: <Board type>:<Board name> <Task ID>" << endl
           << "r/remove Remove a task from a board" << endl
           << "         Options: <Board type>:<Board name> <Task ID>" << endl
           << "v/view   View a task and its description" << endl
           << "         Options: <Board type>:<Board name> <Task ID>" << endl
           << "c/create Add a new board" << endl
           << "         Options: <Board type>:<Board name>" << endl
           << "l/list   List all boards using only their names" << endl
           << "         Options: (none)" << endl
           << "s/star   Star a task" << endl
           << "         Options: <Board name> <Task ID>" << endl
           << "u/unstar Unstar a task" << endl
           << "         Options: <Board name> <Task ID>" << endl
           << "rb       Remove an entire board with its tasks" << endl
           << "         Options: <Board name>" << endl
           << endl
           << "Board types:" << endl
           << "t   Trello" << endl
           << "f   File" << endl;
    } else {
      // Calculate the name
      string name;
      if (argc == 2) {
        name = argv[1];
      } else {
        name = honoka::concatUntilEnd(argv, argc, 1);
      }

      // Find the board
      optional<BoardMetadata> meta = findBoardByName(name, backends);
      if (meta == nullopt) {
        // Failed to get the board
        cout << "Board not found" << endl;
        return 1;
      }

      front->renderBoard(meta->board, meta->backend);
    }
  }
  
  return 0;
}

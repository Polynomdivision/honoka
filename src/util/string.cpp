#include <string>
#include "string.h"

using namespace std;

namespace honoka {
  // Replaces every occurence of @what with @to in @str
  string replace(string &str, string what, string to) {
    string::size_type n;
    while ((n = str.find(what)) != string::npos) {
      str.replace(n, 1, to);
    }

    return str;
  }

  // Appends @pad to @str, until `str.size() == length`
  string rightPad(string str, string pad, int length) {
    while (str.size() <= length)
      str += pad;

    return str;
  }

  // Concats each element in @argv, starting from @start to @argc.
  string concatUntilEnd(char **argv, int argc, unsigned int start) {
    string ret;

    // If we only have one aegument, then we'll just return that
    if (start + 1 == argc) return string(argv[start]);
  
    for (unsigned int i = start; i < argc; i++) {
      // To prevent `honoka add @coding This is a test` to result in
      // ' This is a test', we'll need to *not* prepend a space if we
      // process the first word
      if (i == start)
        ret += string(argv[i]);
      else
        ret += " " + string(argv[i]);
    }
 
    return ret;
  }
};

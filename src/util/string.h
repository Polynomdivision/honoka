#pragma once
#include <string>

using namespace std;

namespace honoka {
  string replace(string &str, string what, string to);
  string rightPad(string str, string pad, int length);
  string concatUntilEnd(char **argv, int argc, unsigned int start);
};

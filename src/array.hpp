#pragma once
#include <vector>
#include <functional>

using namespace std;

// Just like JavaScript's filter.
// Only adds an element of input if `func(el) == true`
template <typename I>
vector<I> filter(vector<I> &input, function<bool(const I)> func) {
  vector<I> out;

  for (auto &el : input) {
    if (func(el))
      out.push_back(el);
  }

  return out;
}

// Checks if element in contained in list.
template <typename T>
inline bool contains(const T &element, vector<T> &list) {
  // Ignore lists that are empty
  if (list.size() == 0)
    return false;

  for (auto &list_el : list)
    if (list_el == element)
      return true;

  return false;
}

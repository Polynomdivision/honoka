#pragma once
#include <iostream>

using namespace std;

#ifndef RELEASE

#define DEBUG(x) cout << "[DEBUG] " << x << endl;

#else

#define DEBUG(x)

#endif // RELEASE

#ifndef RELEASE

// For unfinished or partially done functionality
#define _STUB cout << __FILE__ << "::" << __FUNCTION__ << ": stub" << endl;
#define _SEMI_STUB(reason) cout << __FILE__ << "::" << __FUNCTION__ \
  << ": semi-stub: " << reason << endl;

// Other kinds of messages
#define NOT_SUPPORTED(what, where) cout << what << " is not supported on " \
  << where << endl \
  << ":(" << endl;

#else

#define _STUB
#define _SEMI_STUB(x)
#define NOT_SUPPORTED(what, where)

#endif // RELEASE

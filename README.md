# Honoka - A CLI for all your TODO needs
Heavily inspired by [taskbook](https://github.com/klauscfhq/taskbook)

## Supported backends
- Trello
- A plain file

## Build requirements
- libcurl

### Build
Build using `make release`

### Debug
Build using `make debug`

## Usage
TODO

